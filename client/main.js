import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
import {Mongo} from 'meteor/mongo';
import {Session} from 'meteor/session';

import './main.html';

var Resolutions = new Mongo.Collection('resolutions');

Template.body.helpers({
    resolutions () {
        if(Session.get('hideFinished')) {
            return Resolutions.find({checked: {$ne: true}});
        } else {
            return Resolutions.find();
        }
    },
    hideFinished () {
        return Session.get('hideFinished');
    }
});

Template.body.events({
    'submit .new-resolution'(event) {
        var title = event.target.title.value;

        Meteor.call('addResolution', title);

        event.target.title.value = '';
        return false;
    },
    'change .hide-finished'(event) {
        Session.set('hideFinished', event.target.checked);
    }
});

Template.resolution.helpers({
    isOwner() {
        return this.owner == Meteor.userId();
    }
});

Template.resolution.events({
    'click .toggle-checked'() {
        Meteor.call('updateResolution', this._id, !this.checked);
    },
    'click .toggle-private'() {
        Meteor.call('setPrivate', this._id, !this.private);
    },
    'click .delete'() {
        Meteor.call('removeResolution', this._id);
    }
});

Accounts.ui.config({
	 passwordSignupFields: "USERNAME_ONLY"
});