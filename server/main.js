import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

var Resolutions = new Mongo.Collection('resolutions');

Meteor.startup(() => {
    // code to run on server at startup
});

Meteor.methods({
    addResolution(title) {
        Resolutions.insert({
            title,
            createdAt: new Date(),
            owner    : Meteor.userId()
        });
    },

    updateResolution(id, checked) {
        Resolutions.update(id, {$set: {checked: checked}});
    },

    setPrivate(id, private) {
        var res = Resolutions.findOne(id);
        if (res.owner !== Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }
        Resolutions.update(id, {$set: {private: private}});
    },

    removeResolution(id) {
        var res = Resolutions.findOne(id);

        if(res.owner !== Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        Resolutions.remove(id);
    }
});